﻿using System;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using System.Net.Http;
using System.Diagnostics;
using Newtonsoft.Json.Linq;
using Xamarin.Essentials;
using Acr.UserDialogs;

namespace MapsXamarin
{
    public partial class MainPage : ContentPage
    {
        private ArrayList routePoints;

        public MainPage()
        {
            InitializeComponent();

            routePoints = new ArrayList();
        }

        private async void Calculate_Route_OnClicked(object sender, EventArgs e)
        {
            string originWayPoint = "";
            Location location = null; 

            try
            { 
                var locations = await Geocoding.GetLocationsAsync(origenRuta.Text);
                location = locations?.FirstOrDefault();
                originWayPoint = $"{location.Latitude.ToString().Replace(",", ".")},{location.Longitude.ToString().Replace(",", ".")}";
            } catch (Exception exception)
            {
                Debug.WriteLine(exception.Message);
                origenRuta.Text = "";
                UserDialogs.Instance.Toast("Dirección origen invalida!", TimeSpan.FromMilliseconds(2000));
            }

            if(location != null)
            {
                try
                {
                    var destinationLocations = await Geocoding.GetLocationsAsync(destinoRuta.Text);
                    var destinationLocation = destinationLocations?.FirstOrDefault();
                    var destinationWayPoint = $"{destinationLocation.Latitude.ToString().Replace(",", ".")},{destinationLocation.Longitude.ToString().Replace(",", ".")}";

                    await GetRouteJSON(originWayPoint, destinationWayPoint);
                }
                catch (Exception exception)
                {
                    Debug.WriteLine(exception.Message);
                    destinoRuta.Text = "";
                    UserDialogs.Instance.Toast("Dirección destino invalida!", TimeSpan.FromMilliseconds(2000));
                }
            }
        } 

        private async Task GetRouteJSON(string origin, string destination)
        {
            HttpClient client = new HttpClient();
            string Url = $"http://dev.virtualearth.net/REST/v1/Routes?wp.0={origin}&wp.1={destination}&routePathOutput=Points&tl=1.0&key=AvGdGjyf5hLeAEZQB0ajThVfKuTIq4mafxysAn6E8xO9q1FraWoG9XiLawmzIbqM";
            string content = await client.GetStringAsync(Url);

            JObject json = JObject.Parse(content);
            JArray values = (JArray)json["resourceSets"];
            var resources = (JArray)values[0].ToObject<JObject>()["resources"];
            var routePath = resources[0].ToObject<JObject>()["routePath"];
            var line = routePath["line"];
            var coordinates = (JArray)line["coordinates"];

            GetPointsArray(coordinates);
            SetRouteAsync();
        }

        private void GetPointsArray(JArray coordinates)
        {
            foreach (var coordinate in coordinates.Children())
            {
                routePoints.Add(new Position(coordinate[0].ToObject<double>(), coordinate[1].ToObject<double>()));
            }
        }

        private void SetRouteAsync()
        {
            foreach (Position pointPosition in routePoints)
            {
                MapView.RouteCoordinates.Add(pointPosition);
            }

            MapView.MoveToRegion(MapSpan.FromCenterAndRadius((Position)routePoints[routePoints.Count - 1], Distance.FromMeters(1500)));
            MapView.ChangeProperty();
            ClearArrayLists();
        }

        private void ClearArrayLists()
        {
            routePoints.Clear();
            MapView.RouteCoordinates.Clear();
        }

        public async Task GrantPermissions()
        {
            await CrossPermissions.Current.RequestPermissionsAsync(Permission.Location);
        }

        private void Street_OnClicked(object sender, EventArgs e)
        {
            MapView.MapType = MapType.Street;
        }


        private void Hybrid_OnClicked(object sender, EventArgs e)
        {
            MapView.MapType = MapType.Hybrid;
        }

        private void Satellite_OnClicked(object sender, EventArgs e)
        {
            MapView.MapType = MapType.Satellite;
        }

        private void Clear_Text(object sender, EventArgs args)
        {
            Entry clickedEntry = (Entry)sender;

            clickedEntry.HorizontalTextAlignment = TextAlignment.Start;
            clickedEntry.Text = "";
        }
    }
}
