﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms.Maps;

namespace MapsXamarin
{
    public class MyMap : Map
    {
        public List<Position> RouteCoordinates { get; set; }

        public MyMap()
        {
            RouteCoordinates = new List<Position>();
        }

        internal void ChangeProperty()
        {
            OnPropertyChanged();
        }
    }
}
