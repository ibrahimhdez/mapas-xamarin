﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Android.Content;
using Android.Gms.Maps.Model;
using MapsXamarin;
using MapsXamarin.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Maps.Android;

[assembly: ExportRenderer(typeof(MyMap), typeof(MyMapRenderAndroid))]
namespace MapsXamarin.Droid
{
    public class MyMapRenderAndroid : MapRenderer
    {
        List<Position> routeCoordinates;
        MyMap myMap; 

        public MyMapRenderAndroid(Context context) : base(context)
        {

        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (e.PropertyName == "ChangeProperty")
            {
                RefreshPolyneMap();
            }
        }

        private void RefreshPolyneMap()
        {
            if (myMap != null)
            {
                routeCoordinates = myMap.RouteCoordinates;
                Control.GetMapAsync(this);
            }
        }

        protected override void OnElementChanged(Xamarin.Forms.Platform.Android.ElementChangedEventArgs<Map> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null)
            {
                // Unsubscribe
            }

            if (e.NewElement != null)
            {
                myMap = (MyMap)e.NewElement;
                routeCoordinates = myMap.RouteCoordinates;
                Control.GetMapAsync(this);
            }
        }

        protected override void OnMapReady(Android.Gms.Maps.GoogleMap map)
        {
            base.OnMapReady(map);

            var polylineOptions = new PolylineOptions();
            polylineOptions.InvokeColor(0x66FF0000);

            foreach (var position in routeCoordinates)
            {
                polylineOptions.Add(new LatLng(position.Latitude, position.Longitude));
            }
            NativeMap.AddPolyline(polylineOptions);
        }
    }
}
